package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement){
        if (statement == null || statement == "") return null;
        statement = statement.replaceAll(" ", "");
        try {
            if (statement.contains("(")
                    && statement.indexOf('(') < statement.indexOf(')')) {
                String subStatement = statement.substring(statement.indexOf('(') + 1, statement.indexOf(')'));
                if(subStatement.contains("(") || subStatement.contains(")")) return null;
                if (checkStatement(subStatement)) {
                    subStatement = chooseOperation(subStatement);
                    statement = statement.replace(statement.substring(statement.indexOf('('), statement.indexOf(')') + 1), subStatement);
                }
            } else {
                if (checkStatement(statement)) {
                    statement = chooseOperation(statement);
                } else return null;
            }
            if (checkStatement(statement)) {
                statement = chooseOperation(statement);
            }
        }catch (ArithmeticException e) {
            return null;
        }

        if(statement != null && statement.endsWith(".0")){
            statement = statement.replace(".0", "");
        }

        return statement;
    }

    public boolean checkStatement(String statement) {
        if(!statement.contains("++") && !statement.contains("--") && !statement.contains("**")
                && !statement.contains("//") && !statement.contains(",") && !statement.contains("..")) {
            return true;
        }else {
            return false;
        }
    }

    public String chooseOperation(String statement) {
        while (statement.contains("*") && statement.contains("/")) {
            if (statement.indexOf('*') < statement.indexOf('/')) {
                statement = calculation(statement, '*');
            } else {
                statement = calculation(statement, '/');
            }
        }
        if(statement.contains("*") && !statement.contains("/")){
            statement = calculation(statement, '*');
        }
        if(statement.contains("/") && !statement.contains("*")){
            statement = calculation(statement, '/');
        }
        if(statement == null) return null;
        while (statement.contains("+") && statement.contains("-")){
            if(statement.indexOf('+') < statement.indexOf('-') || statement.indexOf('-') == 0){
                statement = calculation(statement, '+');
            }else {
                statement = calculation(statement, '-');
            }
        }
        while(statement.contains("+")){
            statement = calculation(statement, '+');
        }
        while (statement.contains("-")){
            if(statement.indexOf('-')==0) return statement;
            statement = calculation(statement, '-');
        }
        return statement;
    }

    public String calculation(String statement, char ch) {
        while (statement.contains(String.valueOf(ch))) {
            String firstNum = "";
            String secondNum = "";
            String subStatement = statement.substring(0, statement.indexOf(ch));
            int i = subStatement.length()-1;
            while (i >= 0) {
                if (subStatement.charAt(i) != '+' && subStatement.charAt(i) != '*' && subStatement.charAt(i) != '/'
                        && subStatement.charAt(i) != '-') {
                    firstNum = subStatement.charAt(i)+ firstNum;
                    i--;
                } else {
                    if(i == 0 && subStatement.charAt(i) == '-') {
                        firstNum = "-" + firstNum;
                    }
                    break;
                }
            }

            subStatement = statement.substring(statement.indexOf(ch)+1);
            i = 0;
            while (i < subStatement.length()) {
                if (subStatement.charAt(i) != '+' && subStatement.charAt(i) != '-'
                        && subStatement.charAt(i) != '*' && subStatement.charAt(i) != '/') {
                    secondNum = secondNum + subStatement.charAt(i);
                    i++;
                } else {
                    if(i == 0 && subStatement.charAt(0) == '-') {
                        secondNum = "-" + secondNum;
                        i++;
                    }else break;
                }
            }

            Double doubleResult;
            Integer integerResult;
            switch (ch){
                case '*' : {
                    if(firstNum.contains(".") || secondNum.contains(".")) {
                        doubleResult = Double.parseDouble(firstNum) * Double.parseDouble(secondNum);
                        statement = statement.replace((firstNum+"*"+secondNum), doubleResult.toString());
                    } else {
                        integerResult = Integer.parseInt(firstNum) * Integer.parseInt(secondNum);
                        statement = statement.replace((firstNum + "*" + secondNum), String.valueOf(integerResult));
                    }
                    break;
                }
                case '/' : {
                    try {
                        if (Integer.parseInt(secondNum) == 0) return null;
                        doubleResult = Double.parseDouble(firstNum) / Double.parseDouble(secondNum);
                        statement = statement.replace((firstNum + "/" + secondNum), doubleResult.toString());
                    }catch (ArithmeticException e) {
                        return null;
                    }
                    break;
                }
                case '+' : {
                    if(firstNum.contains(".") || secondNum.contains(".")) {
                        doubleResult = Double.parseDouble(firstNum) + Double.parseDouble(secondNum);
                        statement = statement.replace((firstNum+"+"+secondNum), doubleResult.toString());
                    } else {
                        integerResult = Integer.parseInt(firstNum) + Integer.parseInt(secondNum);
                        statement = statement.replace((firstNum + "+" + secondNum), String.valueOf(integerResult));
                    }
                    break;
                }

                case '-' : {
                    if(firstNum.contains(".") || secondNum.contains(".")) {
                        doubleResult = Double.parseDouble(firstNum) - Double.parseDouble(secondNum);
                        statement = statement.replace((firstNum+"-"+secondNum), doubleResult.toString());
                    } else {
                        integerResult = Integer.parseInt(firstNum) - Integer.parseInt(secondNum);
                        statement = statement.replace((firstNum + "-" + secondNum), String.valueOf(integerResult));
                    }
                    break;
                }
            }

            if(statement.charAt(0)=='-') return statement;


        }

        return statement;
    }


}
