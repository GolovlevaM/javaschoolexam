package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
       try{
                Collections.sort(inputNumbers);
                int h = 0;
                 int l = 0;
                while (l< inputNumbers.size()){
                    l+=++h;
                }
                if(l != inputNumbers.size()) throw new CannotBuildPyramidException();
                int[][] result = new int[h][2 * h - 1];

                l = 0;
                for (int i = 0; i < result.length; i++) {
                    int k = 0;
                    for (int j = result[i].length - result.length - i; k < i + 1 && j < result[i].length; j += 2, k++, l++) {
                        result[i][j] = inputNumbers.get(l);
                    }
                }

                return result;
            }catch (Exception e){
                throw new CannotBuildPyramidException();
            }catch (OutOfMemoryError o) {
                throw new CannotBuildPyramidException();
            }
    }


}
